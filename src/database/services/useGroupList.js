import React, { useEffect } from 'react'
import { db } from '../config'

export function useGroupList() {
  const [error] = React.useState(false)
  const [loading, setLoading] = React.useState(true)
  const [groupList, setGroupList] = React.useState([])

  useEffect(() => {
    const unsubscribe = db.collection('groups').onSnapshot(function(querySnapshot) {
      let list = []
      querySnapshot.forEach(function(doc) {
        const id = doc.id
        const data = doc.data()
        const listItem = {
          id: id,
          name: data.name,
          customQuizName: data.customQuizName,
          customThemeColors: data.slogan,
          thumbnail: data.thumbnail,
        }
        list.push(listItem)
      })
      setLoading(false)
      setGroupList(list)
    })

    return () => unsubscribe()
  }, [])

  return {
    error,
    groupList,
  }
}

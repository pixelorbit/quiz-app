import React, { useEffect } from 'react'

export function usePopStateRedirect() {
  const [popStateRedirect, setPopStateRedirect] = React.useState(false)

  useEffect(() => {
    window.history.pushState(null, document.title, window.location.href)
    window.addEventListener('popstate', function(event) {
      window.history.pushState(null, document.title, window.location.href)
      setPopStateRedirect(true)

      setTimeout(() => {
        setPopStateRedirect(false)
      }, 100)
    })
  }, [])

  return {
    popStateRedirect,
  }
}

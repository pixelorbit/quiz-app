import React from 'react'

export function usePushNotification() {
  const [icon] = React.useState('/friendsquiz-favicon.png')

  const triggerNotification = (title, message) => {

    if (!('Notification' in window)) {
      console.log('This browser does not support desktop notification')
    }
    else if (Notification.permission === 'granted') {
      createNotification(title, message)
    }
    else if (Notification.permission !== 'denied') {
      Notification.requestPermission().then(function(permission) {
        if (permission === 'granted') {
          createNotification(title, message)
        }
      })
    }
  }

  function createNotification(title, message) {
    new Notification(title, {body: message, icon: icon})
  } 

  return {
    triggerNotification
  }
}
# cuiz – Community-Quiz-App with custom questions

**cuiz** is a progressive web app which offers the opportunity to build a private custom quiz with friends, colleagues or the family for preserving your shared memories, insider jokes and fun facts. Our vision is to create a highly personal, valuable and relevant experience for the communcity members. 

## Technology Stack

* React – Fundament for building the single-page-application
* Styled components – Provides styling & theming with CSS-in-JS
* Framer motion – Enables magical animations & transitions
* Firestore - Storage for user-generated data

## Available Scripts

In the project directory, you can run:

### `yarn start 👷‍♂️` 

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test 🧪` 

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build 📦` 

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

### `firebase deploy 🚀` 

Deploys the app to firebase cloud. Run `yarn build` first to upload the last production version. 

#### Default:

`firebase deploy --only hosting:default`

#### Public:

`firebase deploy --only hosting:public`

#### Stage:

`firebase deploy --only hosting:stage`





